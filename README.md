[![Testspace](https://www.testspace.com/img/Testspace.png)](https://www.testspace.com)

***

## Getting Started Examples 

Sample demonstrates techniques for using Testspace (our `hello world`):
  * Using a Testspace Project that is `connected` with this GitHub Repo
  * Using 2 Online CI services for demonstration purposes only
  * Refer to our [Getting Started](https://help.testspace.com/getting-started) help articles for more information
  
***

[Simple Desktop Example](https://help.testspace.com/getting-started:desktop-example):

`Published content` at https://samples.testspace.com/projects/getting.started.

[![Space Health](https://samples.testspace.com/spaces/1491/badge?token=52678fdf02d907a83d9abdfc99e5b300f957e8af)](https://samples.testspace.com/spaces/1491 "Test Cases")
[![Space Metric](https://samples.testspace.com/spaces/1491/metrics/3405/badge?token=e8f618f534c9d190e60acef28fc254085c267ec1)](https://samples.testspace.com/spaces/1491/schema/Code%20Coverage "Code Coverage (lines)")
[![Space Metric](https://samples.testspace.com/spaces/1491/metrics/3406/badge?token=1f4b9a285a57e23fc03ede055c111710674b0f75)](https://samples.testspace.com/spaces/1491/schema/Static%20Analysis "Static Analysis (issues)")


Download and configure the *Testspace client*: 

<pre>
mkdir -p $HOME/bin
curl -fsSL https://testspace-client.s3.amazonaws.com/testspace-linux.tgz | tar -zxvf- -C $HOME/bin
testspace config url YOUR-access-token@YOUR-organization-name.testspace.com/YOUR-project-name
</pre>

Push content using *Testspace client*:
<pre>
testspace analysis.xml [tests]results*.xml coverage.xml "YOUR-space-name" 
</pre> 

***

[GitHub CI Example](https://help.testspace.com/getting-started:github-ci-example):

`Published content` at https://samples.testspace.com/projects/testspace-samples:getting.started.

[![Space Health](https://samples.testspace.com/spaces/833/badge)](https://samples.testspace.com/spaces/833 "Test Cases")
[![Space Metric](https://samples.testspace.com/spaces/833/metrics/833/badge)](https://samples.testspace.com/spaces/833/schema/Code%20Coverage "Code Coverage (lines)")
[![Space Metric](https://samples.testspace.com/spaces/833/metrics/834/badge)](https://samples.testspace.com/spaces/833/schema/Static%20Analysis "Static Analysis (issues)")


Download and configure the *Testspace client*: 

<pre>
mkdir -p $HOME/bin
curl -fsSL https://testspace-client.s3.amazonaws.com/testspace-linux.tgz | tar -zxvf- -C $HOME/bin
testspace config url YOUR-subdomain.testspace.com
</pre>

Note that an *access-token* is required when configuring if the repo is private.

Push content using *Testspace client*:
<pre>
testspace analysis.xml [tests]results*.xml coverage.xml 
</pre> 

