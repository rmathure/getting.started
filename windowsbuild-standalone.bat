curl -o testspace-windows.zip https://testspace-client.s3.amazonaws.com/testspace-windows.zip
7z x testspace-windows.zip -aoa

REM Note, using Windows Control Panel | User Accounts | Credentials Manager
REM to contain "access token" credentials (refer to https://help.testspace.com/reference:client-reference)

testspace config url samples.testspace.com/getting.started
testspace -v

testspace analysis.xml [tests]results*.xml coverage.xml windows#windows.build --repo git