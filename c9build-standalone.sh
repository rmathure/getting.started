#!/bin/bash

# Download and configure the Testspace client
mkdir -p $HOME/bin
curl -fsSL https://testspace-client.s3.amazonaws.com/testspace-linux.tgz | tar -zxvf- -C $HOME/bin
# Note, using ~/.netrc to contain "access token" credentials (refer to https://help.testspace.com/reference:client-reference)
testspace config url samples.testspace.com/getting.started
testspace -v

# Push content using Testspace client
testspace analysis.xml [tests]results*.xml coverage.xml my-space#c9.build --repo git